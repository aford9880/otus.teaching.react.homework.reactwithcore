import React, { useState, useEffect } from 'react';
import './App.css';

interface WeatherData {
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}

function App() {
  const [weather, setWeather] = useState<WeatherData[] | null>(null);

  useEffect(() => {
    fetch('https://localhost:7070/weatherforecast')
      .then((response) => response.json())
      .then((data) => setWeather(data))
      .catch((error) => console.error('Ошибка загрузки данных:', error));
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <h1>Weather App</h1>
        {weather ? (
          <div className="weather-container">
            {weather.map((item, index) => (
              <div className="weather-card" key={index}>
                <h2>{item.date}</h2>
                <p>Temperature: {item.temperatureC}°C</p>
                <p>Summary: {item.summary}</p>
              </div>
            ))}
          </div>
        ) : (
          <p>Loading weather data...</p>
        )}
      </header>
    </div>
  );
}

export default App;